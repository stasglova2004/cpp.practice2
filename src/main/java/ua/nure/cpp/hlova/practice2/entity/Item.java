package ua.nure.cpp.hlova.practice2.entity;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Represents an entity from the warehouse database.
 * <p>Implements a {@link Comparable<Item>} for sorting by price in ascending order</p>
 *
 * @author Hlova Stanislav
 */
public class Item implements Comparable<Item> {
    private String vendor;
    private String name;
    private String unit;
    private BigDecimal price;
    private BigDecimal weight;

    Item(Builder builder) {
        this.vendor = builder.vendor;
        this.name = builder.name;
        this.unit = builder.unit;
        this.price = builder.price;
        this.weight = builder.weight;
    }

    public Item(String vendor, String name, String unit, BigDecimal price, BigDecimal weight) {
        this.vendor = vendor;
        this.name = name;
        this.unit = unit;
        this.price = price;
        this.weight = weight;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (!Objects.equals(vendor, item.vendor)) return false;
        if (!Objects.equals(name, item.name)) return false;
        if (!Objects.equals(unit, item.unit)) return false;
        if (!Objects.equals(price, item.price)) return false;
        return Objects.equals(weight, item.weight);
    }

    @Override
    public int hashCode() {
        int result = vendor != null ? vendor.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (unit != null ? unit.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (weight != null ? weight.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '{' +
                "vendor='" + vendor + '\'' +
                ", name='" + name + '\'' +
                ", unit='" + unit + '\'' +
                ", price=" + price +
                ", weight=" + weight +
                '}';
    }

    @Override
    public int compareTo(Item item) {
        if (price == null) return -1;
        return price.compareTo(item.price);
    }

    public static class Builder<T extends Builder<T>> {
        private final String vendor;
        private String name = "";
        private String unit = "";
        private final BigDecimal price;
        private BigDecimal weight = new BigDecimal(0);

        public Builder(String vendor, BigDecimal price) {
            this.vendor = Objects.requireNonNull(vendor);
            this.price = Objects.requireNonNull(price);
        }
        public T name(String name){
            this.name=name;
            return self();
        }
        public T unit(String unit){
            this.unit=unit;
            return self();
        }
        public T weight(BigDecimal weight){
            this.weight=weight;
            return self();
        }
        public Item build() {
            return new Item(this);
        }
        protected T self(){
            return (T) this;
        }
    }
}
