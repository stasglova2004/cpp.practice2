package ua.nure.cpp.hlova.practice2;


import ua.nure.cpp.hlova.practice2.comparators.foodItem.FoodItemComparatorByCaloricAndPrice;
import ua.nure.cpp.hlova.practice2.entity.FoodItem;
import ua.nure.cpp.hlova.practice2.entity.Heater;
import ua.nure.cpp.hlova.practice2.entity.Item;
import ua.nure.cpp.hlova.practice2.entity.Patty;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * Demonstrates a working of sort using comparable and comparator implementation on the one-dimensional array.
 *
 * @author Hlova Stanislav
 */
public class Demo {
    /**
     * The point of the running.
     *
     * @param args arguments of the execution for the method.
     */
    public static void main(String[] args) {
        FoodItem[] items = getFoodItems();
        System.out.println("Initial array.");
        printArray(items);
        Arrays.sort(items, new FoodItemComparatorByCaloricAndPrice());
        System.out.println("After sorting by Comparator implementation.");
        printArray(items);

        items = getFoodItems();
        System.out.println("Initial array.");
        printArray(items);
        Arrays.sort(items);
        System.out.println("After sorting by Comparable implementation.");
        printArray(items);
    }

    /**
     * Returns an array with different items.
     *
     * @return an array with different items.
     */
    private static FoodItem[] getFoodItems() {
        FoodItem[] items = new FoodItem[5];
        items[0] = new FoodItem.Builder<>("1234-56",new BigDecimal(100)).name("Cake").unit("kg").weight(new BigDecimal(5)).caloric(1500).build();
        items[1] = new FoodItem.Builder<>("23-4567",new BigDecimal(75)).name("Banana").unit("kg").weight(new BigDecimal(10)).caloric(140).build();
        items[2] = new FoodItem.Builder<>("345-678",new BigDecimal(45)).name("Apple jam").unit("jar").weight(new BigDecimal(15)).caloric(550).build();
        items[3] = new FoodItem.Builder<>("13-78",new BigDecimal(35)).name("Cherry jam").unit("jar").weight(new BigDecimal(15)).caloric(550).build();
        items[4] = new FoodItem.Builder<>("345-432",new BigDecimal(55)).name("Apricot jam").unit("jar").weight(new BigDecimal(15)).caloric(550).build();
        return items;
    }

    /**
     * Prints an array of {@link FoodItem}
     * @param items an array of {@link FoodItem}
     */
    private static void printArray(FoodItem[] items) {
        System.out.println("~~~~~~~~~~~~~~~~~~~~");
        for (FoodItem foodItem : items) {
            System.out.println(foodItem);
        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~");
    }
}
