package ua.nure.cpp.hlova.practice2.comparators.heater;

import ua.nure.cpp.hlova.practice2.entity.Heater;

import java.util.Comparator;

/**
 * Represents a comparator for comparing {@link Heater} by power in ascending order.
 *
 * @author Hlova Stanislav
 */
public class HeaterComparatorByPower implements Comparator<Heater> {
    @Override
    public int compare(Heater o1, Heater o2) {
        if (o1 == null && o2 == null) return 0;
        if (o1 == null) return -1;
        if (o2 == null) return 1;
        return Double.compare(o1.getPower(), o2.getPower());
    }
}
