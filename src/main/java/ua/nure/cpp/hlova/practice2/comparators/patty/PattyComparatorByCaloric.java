package ua.nure.cpp.hlova.practice2.comparators.patty;

import ua.nure.cpp.hlova.practice2.entity.Patty;

import java.util.Comparator;

/**
 * Represents a comparator for comparing {@link Patty} by caloric in ascending order.
 *
 * @author Hlova Stanislav
 */
public class PattyComparatorByCaloric implements Comparator<Patty> {
    @Override
    public int compare(Patty o1, Patty o2) {
        if (o1 == null && o2 == null) return 0;
        if (o1 == null) return -1;
        if (o2 == null) return 1;
        return Double.compare(o1.getCaloric(),o2.getCaloric());
    }
}
