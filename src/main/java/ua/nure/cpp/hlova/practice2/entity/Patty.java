package ua.nure.cpp.hlova.practice2.entity;

import java.math.BigDecimal;

/**
 * Represents an patty-entity.
 * <p>Implements a {@link Comparable<FoodItem>} for sorting by flour type in ascending order</p>
 *
 * @author Hlova Stanislav
 */
public class Patty extends FoodItem {
    private FlourType flourType;

    public Patty(Builder builder) {
        super(builder);
        this.flourType = builder.flourType;
    }

    /**
     * Represents a king of flour.
     *
     * @author Hlova Stanislav
     */
    public enum FlourType {
        FIRST_KIND, SECOND_KIND;
    }


    public Patty(String vendor, String name, String unit, BigDecimal price, BigDecimal weight, double caloric, FlourType flourType) {
        super(vendor, name, unit, price, weight, caloric);
        this.flourType = flourType;
    }

    public FlourType getFlourType() {
        return flourType;
    }

    public void setFlourType(FlourType flourType) {
        this.flourType = flourType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Patty patty = (Patty) o;

        return flourType == patty.flourType;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + flourType.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '{' +
                "vendor='" + getVendor() + '\'' +
                ", name='" + getName() + '\'' +
                ", unit='" + getUnit() + '\'' +
                ", price=" + getPrice() +
                ", weight=" + getWeight() +
                ", caloric=" + getCaloric() +
                ", flourType='" + flourType + '\'' +
                '}';
    }

    @Override
    public int compareTo(Item item) {
        Patty patty = (Patty) item;
        return flourType.compareTo(patty.flourType);
    }
    public static class Builder extends FoodItem.Builder<Builder>{
        private FlourType flourType = FlourType.SECOND_KIND;
        public Builder(String vendor, BigDecimal price) {
            super(vendor, price);
        }
        public Builder flour(FlourType flourType){
            this.flourType = flourType;
            return self();
        }
        @Override
        public Patty build(){
            return new Patty(this);
        }
        @Override
        protected Builder self(){
            return this;
        }
    }
}
