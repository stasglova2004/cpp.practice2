package ua.nure.cpp.hlova.practice2.entity;

import java.math.BigDecimal;

/**
 * Represents an entity, which is one king of the food.
 * <p>Implements a {@link Comparable<FoodItem>} for sorting by caloric in ascending order</p>
 *
 * @author Hlova Stanislav
 */
public class FoodItem extends Item {
    private double caloric;
    FoodItem(Builder builder){
        super(builder);
        this.caloric = builder.caloric;
    }
    public FoodItem(String vendor, String name, String unit, BigDecimal price, BigDecimal weight, double caloric) {
        super(vendor, name, unit, price, weight);
        this.caloric = caloric;
    }

    public double getCaloric() {
        return caloric;
    }

    public void setCaloric(double caloric) {
        this.caloric = caloric;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        FoodItem foodItem = (FoodItem) o;

        return Double.compare(foodItem.caloric, caloric) == 0;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(caloric);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '{' +
                "vendor='" + getVendor() + '\'' +
                ", name='" + getName() + '\'' +
                ", unit='" + getUnit() + '\'' +
                ", price=" + getPrice() +
                ", weight=" + getWeight() +
                ", caloric=" + caloric +
                '}';
    }

    @Override
    public int compareTo(Item item) {
        FoodItem foodItem = (FoodItem) item;
        return Double.compare(caloric, foodItem.caloric);
    }
    public static class Builder<T extends Builder<T>> extends Item.Builder<T>{
        private double caloric = 0d;
        public Builder(String vendor, BigDecimal price) {
            super(vendor, price);
        }
        public T caloric(double caloric){
            this.caloric = caloric;
            return self();
        }
        @Override
        public FoodItem build(){
            return new FoodItem(this);
        }
        @Override
        protected T self(){
            return (T) this;
        }
    }
}
