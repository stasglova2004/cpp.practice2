package ua.nure.cpp.hlova.practice2.comparators.foodItem;

import ua.nure.cpp.hlova.practice2.entity.FoodItem;

import java.util.Comparator;

/**
 * Represents a comparator for comparing {@link FoodItem} by caloric and then by price,
 * if the two objects have the same caloric, in ascending order.
 *
 * @author Hlova Stanislav
 */
public class FoodItemComparatorByCaloricAndPrice implements Comparator<FoodItem> {
    @Override
    public int compare(FoodItem o1, FoodItem o2) {
        if (o1 == null && o2 == null) return 0;
        if (o1 == null) return -1;
        if (o2 == null) return 1;
        int compareResult = Double.compare(o1.getCaloric(), o2.getCaloric());
        if (compareResult != 0) {
            return compareResult;
        }
        if (o1.getPrice() == null) {
            return -1;
        }
        return o1.getPrice().compareTo(o2.getPrice());
    }
}
