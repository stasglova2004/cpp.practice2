package ua.nure.cpp.hlova.practice2.entity;

import java.math.BigDecimal;

/**
 * Represents an heater entity, which can enable and disable.
 * <p>Implements a {@link Comparable<Heater>} for sorting by power in ascending order</p>
 *
 * @author Hlova Stanislav
 */
public class Heater extends Item {
    private double power;
    private boolean enabled;
    Heater(Builder builder){
        super(builder);
        this.power = builder.power;
        this.enabled = builder.enable;
    }
    public Heater(String vendor, String name, String unit, BigDecimal price, BigDecimal weight, double power) {
        super(vendor, name, unit, price, weight);
        this.power = power;
    }

    public double getPower() {
        return power;
    }

    public void setPower(double power) {
        this.power = power;
    }

    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Turn on the heater.
     * <p>Throws an exception, when the heater has already turned on</p>
     *
     * @throws IllegalStateException, when the heater is being trying to turn on twice.
     */
    public void on() {
        if (enabled) throw new IllegalStateException("Can't turn on twice.");
        enabled = true;
    }

    /**
     * Turn off the heater.
     * <p>Throws an exception, when the heater has already turned off</p>
     *
     * @throws IllegalStateException, when the heater is being trying to turn off twice.
     */
    public void off() {
        if (!enabled) throw new IllegalStateException("Can't turn off twice.");
        enabled = false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Heater heater = (Heater) o;

        if (Double.compare(heater.power, power) != 0) return false;
        return enabled == heater.enabled;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(power);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (enabled ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '{' +
                "vendor='" + getVendor() + '\'' +
                ", name='" + getName() + '\'' +
                ", unit='" + getUnit() + '\'' +
                ", price=" + getPrice() +
                ", weight=" + getWeight() +
                ", power=" + power +
                ", enabled=" + enabled +
                '}';
    }

    @Override
    public int compareTo(Item item) {
        Heater heater = (Heater) item;
        return Double.compare(power, heater.power);
    }

    public static class Builder extends Item.Builder<Builder> {
        private double power = 0;
        private boolean enable = false;
        public Builder(String vendor, BigDecimal price) {
            super(vendor, price);
        }
        public Builder power(double power){
            this.power = power;
            return self();
        }
        public Builder enable(boolean enable){
            this.enable = enable;
            return self();
        }
        @Override
        public Heater build(){
            return new Heater(this);
        }
        @Override
        protected Builder self() {
            return this;
        }
    }
}
