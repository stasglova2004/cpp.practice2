package ua.nure.cpp.hlova.practice2.comparators.foodItem;

import ua.nure.cpp.hlova.practice2.entity.FoodItem;

import java.util.Comparator;

/**
 * Represents a comparator for comparing {@link FoodItem} by caloric in ascending order.
 *
 * @author Hlova Stanislav
 */
public class FoodItemComparatorByCaloric implements Comparator<FoodItem> {
    @Override
    public int compare(FoodItem o1, FoodItem o2) {
        if (o1 == null && o2 == null) return 0;
        if (o1 == null) return -1;
        if (o2 == null) return 1;
        return Double.compare(o1.getCaloric(), o2.getCaloric());
    }
}
