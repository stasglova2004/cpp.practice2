package ua.nure.cpp.hlova.practice2.comparators.item;

import ua.nure.cpp.hlova.practice2.entity.Item;

import java.util.Comparator;

/**
 * Represents a comparator for comparing {@link Item} by name in ascending order.
 *
 * @author Hlova Stanislav
 */
public class ItemComparatorByName implements Comparator<Item> {
    @Override
    public int compare(Item o1, Item o2) {
        if (o1 == null && o2 == null) return 0;
        if (o1 == null) return -1;
        if (o2 == null) return 1;
        return o1.getName().compareTo(o2.getName());
    }
}
