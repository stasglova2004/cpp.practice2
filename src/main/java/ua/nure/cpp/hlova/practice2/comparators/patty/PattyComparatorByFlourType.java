package ua.nure.cpp.hlova.practice2.comparators.patty;

import ua.nure.cpp.hlova.practice2.entity.Patty;

import java.util.Comparator;

/**
 * Represents a comparator for comparing {@link Patty} by flour type in ascending order.
 *
 * @author Hlova Stanislav
 */
public class PattyComparatorByFlourType implements Comparator<Patty> {
    @Override
    public int compare(Patty o1, Patty o2) {
        if (o1 == null && o2 == null) return 0;
        if (o1 == null) return -1;
        if (o2 == null) return 1;
        return o1.getFlourType().compareTo(o2.getFlourType());
    }
}
