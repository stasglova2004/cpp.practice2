package ua.nure.cpp.hlova.practice2.entity;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import java.math.BigDecimal;

public class PattyTest {
    @Test
    public void shouldReturnCorrectPatty_whenBuilderUsed(){
        final String vendor = "vendor";
        final String name = "name";
        final String unit = "unit";
        final BigDecimal price = new BigDecimal(10);
        final BigDecimal weight = new BigDecimal(20);
        final double caloric = 400.2d;
        final Patty.FlourType flourType = Patty.FlourType.FIRST_KIND;

        Patty patty = new Patty.Builder(vendor,price).name(name).caloric(caloric).flour(flourType).unit(unit).weight(weight).build();

        assertEquals(vendor,patty.getVendor());
        assertEquals(name,patty.getName());
        assertEquals(unit,patty.getUnit());
        assertEquals(price,patty.getPrice());
        assertEquals(weight,patty.getWeight());
        assertEquals(caloric,patty.getCaloric(),0);
        assertEquals(flourType,patty.getFlourType());
    }
}