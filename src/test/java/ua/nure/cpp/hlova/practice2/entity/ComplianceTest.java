package ua.nure.cpp.hlova.practice2.entity;

import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.junit.Ignore;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spoon.Launcher;
import spoon.SpoonAPI;
import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtPackage;
import spoon.reflect.declaration.CtType;
import spoon.reflect.declaration.CtTypeInformation;
import spoon.reflect.declaration.ModifierKind;
import spoon.reflect.path.CtRole;
import spoon.reflect.reference.CtTypeReference;
import spoon.reflect.visitor.Filter;

class ComplianceTest {
    static final Logger LOG = LoggerFactory.getLogger(ComplianceTest.class);
    static final String JAVA_UTIL = "java.util";
    static final String PRACTICE_PACKAGE = "practice2";
    static final String BASE_PACKAGE_TEMPLATE = "ua.nure.cpp.<your last name>" + "." + PRACTICE_PACKAGE;
    static final String ENTITY_PACKAGE = "entity";
    private static CtModel ctModel;
    private static Set<String> nonAbstractEntities;

    static String basePackage;

    @BeforeAll
    static void init() {
        SpoonAPI spoon = new Launcher();
        spoon.addInputResource("src/main/java/");
        ctModel = spoon.buildModel();

        Optional<String> any = ctModel.getAllPackages().stream()
                .map(CtPackage::getQualifiedName)
                .filter(pfqn -> pfqn.endsWith(PRACTICE_PACKAGE))
                .findAny();
        assertTrue(any.isPresent(), "Can not find " +
                BASE_PACKAGE_TEMPLATE);
        basePackage = any.get();
        LOG.info("Detected basePackage: {}", basePackage);
    }

    @Test
    void testCompliancePackageNaming() {
        Matcher m = Pattern.compile("ua\\.nure\\.cpp\\.[a-z&&\\D]+?\\.practice2").matcher(basePackage);
        assertTrue(m.matches(),
                "Base package must be '" + BASE_PACKAGE_TEMPLATE + "'.\n" +
                        "Where <your_last_name> is your last name in lower case without digits, " +
                        "but was: " + basePackage);
        assertNotEquals(BASE_PACKAGE_TEMPLATE, basePackage,
                "You do not have name 'name', rename package.");
    }

    @Test
    void testComplianceTopLevelClass() {
        List<CtType<?>> entityTypes = ctModel.filterChildren((Filter<CtType<?>>) el ->
                        el.getQualifiedName().startsWith(basePackage + "." + ENTITY_PACKAGE))
                .select((Filter<CtType<?>>) el -> el.getRoleInParent().equals(CtRole.CONTAINED_TYPE))
                .list();
        entityTypes = entityTypes.stream().distinct().toList();
        List<String> entities = entityTypes.stream().map(CtTypeInformation::getQualifiedName).distinct().toList();
        LOG.info("Detected entities: {}", entities);
        // Entities must be placed in entity package
        assertFalse(entities.isEmpty(),
                "Entities must be placed in entity package.");

        HashMap<CtType<?>, CtTypeReference<?>> hierarchies = new HashMap<>();
        entityTypes.stream().filter(Objects::nonNull).forEach(e -> hierarchies.put(e, e.getSuperclass()));
        LOG.info("Detected hierarchy: {}", entriesToString(hierarchies));

        Set<Map.Entry<CtType<?>, CtTypeReference<?>>> topClasses = hierarchies.entrySet().stream()
                .filter(e -> e.getValue() != null)
                .collect(Collectors.toSet());
        assertTrue(1 < topClasses.size(),
                "The hierarchy must have at least 3 levels");
    }

    // java.util is prohibited

    @Test
    void testComplianceJavaUtilIsForbidden() {
        List<String> forbiddenClasses = ctModel
                .filterChildren((Filter<CtTypeReference<?>>) el ->
                        el.getQualifiedName().startsWith(JAVA_UTIL))
                .list()
                .stream()
                .map(el -> ((CtTypeReference<?>) el).getQualifiedName())
                .distinct()
                .filter(el -> el.startsWith(JAVA_UTIL) &&
                        !(el.endsWith("Objects") ||
                                el.endsWith("Arrays") ||
                                el.endsWith("Comparator"))
                ).toList();
        LOG.debug("forbiddenClasses: {}", forbiddenClasses);
        assertTrue(forbiddenClasses.isEmpty(), "'java.util' package is forbidden " +
                "except 'java.util.Objects', 'java.util.Comparator'. " +
                "Your forbidden classes: " + forbiddenClasses);
    }
    private static List<String> entriesToString(HashMap<CtType<?>, CtTypeReference<?>> hierarchies) {
        return hierarchies
                .entrySet().stream()
                .map(entry -> entry.getKey().getReference() + " = " + entry.getValue())
                .toList();
    }

    static Set<String> getAllNonAbstractEntities() {
        if (nonAbstractEntities == null) {
            Collection<CtType<?>> elements = ctModel.getElements(
                    element -> element.getModifiers().stream()
                            .filter(m -> m.equals(ModifierKind.ABSTRACT))
                            .toList()
                            .isEmpty() &&
                            element.getQualifiedName().startsWith(basePackage + "." + ENTITY_PACKAGE)  &&
                            !element.getRoleInParent().equals(CtRole.TYPE_MEMBER)
            );
            nonAbstractEntities = elements.stream()
//                    .filter(t -> t.getRoleInParent().getSuperRole() != CtRole.NESTED_TYPE)
                    .map(CtTypeInformation::getQualifiedName)
                    .collect(Collectors.toSet());
            // Hierarchy must have more than 1 class
            assertTrue(1 < nonAbstractEntities.size(),
                    "Hierarchy must have more then 1 class");
        }
        return nonAbstractEntities;
    }

}