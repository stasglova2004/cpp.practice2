package ua.nure.cpp.hlova.practice2.comparators.heater;

import static org.junit.Assert.assertArrayEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;
import ua.nure.cpp.hlova.practice2.entity.Heater;

import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class)
public class HeaterComparatorsTest {
    @Mock
    private Heater heater1;
    @Mock
    private Heater heater2;
    @Mock
    private Heater heater3;
    @Mock
    private Heater heater4;

    @Test
    public void shouldSortHeatersByPowerInAscending_whenUnsortedArrayGiven() {
        final Heater[] expected = new Heater[]{heater1, heater2, heater3};
        when(heater1.getPower()).thenReturn(100d);
        when(heater2.getPower()).thenReturn(200d);
        when(heater3.getPower()).thenReturn(300d);
        Heater[] actual = new Heater[]{heater3, heater2, heater1};

        Arrays.sort(actual, new HeaterComparatorByPower());

        assertArrayEquals("The array must be sorted in ascending order by power.", expected, actual);
    }

    @Test
    public void shouldSortHeatersByPowerAndNameInAscending_whenUnsortedArrayGiven() {
        final Heater[] expected = new Heater[]{heater1, heater2, heater3, heater4};
        when(heater1.getPower()).thenReturn(200d);
        when(heater1.getName()).thenReturn("a");
        when(heater2.getPower()).thenReturn(200d);
        when(heater2.getName()).thenReturn("b");
        when(heater3.getPower()).thenReturn(300d);
        when(heater3.getName()).thenReturn("a");
        when(heater4.getPower()).thenReturn(300d);
        when(heater4.getName()).thenReturn("n");
        Heater[] actual = new Heater[]{heater3, heater2, heater1, heater4};

        Arrays.sort(actual, new HeaterComparatorByPowerAndName());

        assertArrayEquals("The array must be sorted in ascending order by power and then by name.", expected, actual);
    }
}
