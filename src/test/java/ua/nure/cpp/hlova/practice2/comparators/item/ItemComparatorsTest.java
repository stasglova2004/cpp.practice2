package ua.nure.cpp.hlova.practice2.comparators.item;

import static org.junit.Assert.assertArrayEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;
import ua.nure.cpp.hlova.practice2.entity.Item;

import java.math.BigDecimal;
import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class)
public class ItemComparatorsTest {
    @Mock
    private Item item1;
    @Mock
    private Item item2;
    @Mock
    private Item item3;
    @Mock
    private Item item4;
    @Mock
    private Item item5;

    @Test
    public void shouldSortItemByNameInAscending_whenUnsortedArrayGiven() {
        final Item[] expected = new Item[]{item1, item2, item3};
        when(item1.getName()).thenReturn("a");
        when(item2.getName()).thenReturn("b");
        when(item3.getName()).thenReturn("c");
        Item[] actual = new Item[]{item3, item2, item1};

        Arrays.sort(actual, new ItemComparatorByName());

        assertArrayEquals("The array must be sorted in ascending order by name.", expected, actual);
    }

    @Test
    public void shouldSortItemByPriceInAscending_whenUnsortedArrayGiven() {
        final Item[] expected = new Item[]{item1, item2, item3};
        when(item1.getPrice()).thenReturn(new BigDecimal(1));
        when(item2.getPrice()).thenReturn(new BigDecimal(2));
        when(item3.getPrice()).thenReturn(new BigDecimal(3));
        Item[] actual = new Item[]{item3, item2, item1};

        Arrays.sort(actual, new ItemComparatorByPrice());

        assertArrayEquals("The array must be sorted in ascending order by price.", expected, actual);
    }

    @Test
    public void shouldSortItemByPriceAndNameInAscending_whenUnsortedArrayGiven() {
        final Item[] expected = new Item[]{item1, item2, item3, item4, item5};
        when(item1.getPrice()).thenReturn(new BigDecimal(1));
        when(item2.getPrice()).thenReturn(new BigDecimal(1));
        when(item3.getPrice()).thenReturn(new BigDecimal(3));
        when(item4.getPrice()).thenReturn(new BigDecimal(3));
        when(item5.getPrice()).thenReturn(new BigDecimal(3));
        when(item1.getName()).thenReturn("a");
        when(item2.getName()).thenReturn("b");
        when(item3.getName()).thenReturn("a");
        when(item4.getName()).thenReturn("c");
        when(item5.getName()).thenReturn("d");
        Item[] actual = new Item[]{item3, item2, item1, item5, item4};

        Arrays.sort(actual, new ItemComparatorByPrice().thenComparing(new ItemComparatorByName()));

        assertArrayEquals("The array must be sorted in ascending order by price.", expected, actual);
    }
}
