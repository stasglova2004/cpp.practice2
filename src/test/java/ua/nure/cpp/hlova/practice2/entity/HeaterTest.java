package ua.nure.cpp.hlova.practice2.entity;


import static org.junit.Assert.assertEquals;
import org.junit.Test;

import java.math.BigDecimal;

public class HeaterTest {
    @Test
    public void shouldReturnCorrectHeater_whenBuilderUsed(){
        final String vendor = "vendor";
        final String name = "name";
        final String unit = "unit";
        final BigDecimal price = new BigDecimal(10);
        final BigDecimal weight = new BigDecimal(20);
        final double power = 120.4d;
        final boolean enable = true;

        Heater heater = new Heater.Builder(vendor,price).name(name).unit(unit).weight(weight).power(power).enable(true).build();

        assertEquals(vendor,heater.getVendor());
        assertEquals(name,heater.getName());
        assertEquals(unit,heater.getUnit());
        assertEquals(price,heater.getPrice());
        assertEquals(weight,heater.getWeight());
        assertEquals(power,heater.getPower(),0);
        assertEquals(enable,heater.isEnabled());
    }
}