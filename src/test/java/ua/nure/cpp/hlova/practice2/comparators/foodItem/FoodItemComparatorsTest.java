package ua.nure.cpp.hlova.practice2.comparators.foodItem;

import static org.junit.Assert.assertArrayEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;
import ua.nure.cpp.hlova.practice2.entity.FoodItem;

import java.math.BigDecimal;
import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class)
public class FoodItemComparatorsTest {
    @Mock
    private FoodItem item1;
    @Mock
    private FoodItem item2;
    @Mock
    private FoodItem item3;
    @Mock
    private FoodItem item4;

    @Test
    public void shouldSortFoodItemByCaloricInAscending_whenUnsortedArrayGiven() {
        final FoodItem[] expected = new FoodItem[]{item3, item2, item1};
        when(item1.getCaloric()).thenReturn(300d);
        when(item2.getCaloric()).thenReturn(200d);
        when(item3.getCaloric()).thenReturn(100d);
        FoodItem[] actual = new FoodItem[]{item1, item2, item3};

        Arrays.sort(actual, new FoodItemComparatorByCaloric());

        assertArrayEquals("The array must be sorted in ascending order by caloric.", expected, actual);
    }

    @Test
    public void shouldSortFoodItemByCaloricAndPriceInAscending_whenUnsortedArrayGiven() {
        final FoodItem[] expected = new FoodItem[]{item1, item2, item3, item4};
        when(item1.getCaloric()).thenReturn(100d);
        when(item1.getPrice()).thenReturn(new BigDecimal(100));
        when(item2.getCaloric()).thenReturn(100d);
        when(item2.getPrice()).thenReturn(new BigDecimal(200));
        when(item3.getCaloric()).thenReturn(300d);
        when(item3.getPrice()).thenReturn(new BigDecimal(40));
        when(item4.getCaloric()).thenReturn(300d);
        when(item4.getPrice()).thenReturn(new BigDecimal(60));
        FoodItem[] actual = new FoodItem[]{item3, item2, item1, item4};

        Arrays.sort(actual, new FoodItemComparatorByCaloricAndPrice());

        assertArrayEquals("The array must be sorted in ascending order by caloric and then by price, if caloric equals.", expected, actual);
    }
}
