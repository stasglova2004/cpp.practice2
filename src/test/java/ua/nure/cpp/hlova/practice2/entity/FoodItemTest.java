package ua.nure.cpp.hlova.practice2.entity;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import java.math.BigDecimal;

public class FoodItemTest {
    @Test
    public void shouldReturnCorrectFoodItem_whenBuilderUsed(){
        final String vendor = "vendor";
        final String name = "name";
        final String unit = "unit";
        final BigDecimal price = new BigDecimal(10);
        final BigDecimal weight = new BigDecimal(20);
        final double caloric = 400.2d;

        FoodItem foodItem = new FoodItem.Builder<>(vendor,price).name(name).caloric(caloric).unit(unit).weight(weight).build();

        assertEquals(vendor,foodItem.getVendor());
        assertEquals(name,foodItem.getName());
        assertEquals(unit,foodItem.getUnit());
        assertEquals(price,foodItem.getPrice());
        assertEquals(weight,foodItem.getWeight());
        assertEquals(caloric,foodItem.getCaloric(),0);
    }
}