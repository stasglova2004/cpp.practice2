package ua.nure.cpp.hlova.practice2.entity;


import static org.junit.Assert.assertEquals;
import org.junit.Test;

import java.math.BigDecimal;

public class ItemTest {
    @Test
    public void shouldReturnCorrectItem_whenBuilderUsed(){
        final String vendor = "vendor";
        final String name = "name";
        final String unit = "unit";
        final BigDecimal price = new BigDecimal(10);
        final BigDecimal weight = new BigDecimal(20);

        Item item = new Item.Builder<>(vendor,price).name(name).unit(unit).weight(weight).build();

        assertEquals(vendor,item.getVendor());
        assertEquals(name,item.getName());
        assertEquals(unit,item.getUnit());
        assertEquals(price,item.getPrice());
        assertEquals(weight,item.getWeight());
    }
}