package ua.nure.cpp.hlova.practice2.comparators.patty;

import static org.junit.Assert.assertArrayEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;
import ua.nure.cpp.hlova.practice2.entity.Patty;

import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class)
public class PattyComparatorsTest {
    @Mock
    private Patty patty1;
    @Mock
    private Patty patty2;
    @Mock
    private Patty patty3;
    @Mock
    private Patty patty4;

    @Test
    public void shouldSortPattyByCaloricInAscending_whenUnsortedArrayGiven() {
        final Patty[] expected = new Patty[]{patty1, patty2, patty3};
        when(patty1.getCaloric()).thenReturn(100d);
        when(patty2.getCaloric()).thenReturn(200d);
        when(patty3.getCaloric()).thenReturn(300d);
        Patty[] actual = new Patty[]{patty3, patty2, patty1};

        Arrays.sort(actual, new PattyComparatorByCaloric());

        assertArrayEquals("The array must be sorted in ascending order by caloric.", expected, actual);
    }

    @Test
    public void shouldSortPattyByFlourTypeInAscending_whenUnsortedArrayGiven() {
        final Patty[] expected = new Patty[]{patty1, patty2, patty3};
        when(patty1.getFlourType()).thenReturn(Patty.FlourType.FIRST_KIND);
        when(patty2.getFlourType()).thenReturn(Patty.FlourType.FIRST_KIND);
        when(patty3.getFlourType()).thenReturn(Patty.FlourType.SECOND_KIND);
        Patty[] actual = new Patty[]{patty3, patty1, patty2};

        Arrays.sort(actual, new PattyComparatorByFlourType());

        assertArrayEquals("The array must be sorted in ascending order by flour type.", expected, actual);
    }

    @Test
    public void shouldSortPattyByCaloricAndFlourTypeInAscending_whenUnsortedArrayGiven() {
        final Patty[] expected = new Patty[]{patty1, patty2, patty3, patty4};
        when(patty1.getCaloric()).thenReturn(200d);
        when(patty2.getCaloric()).thenReturn(200d);
        when(patty3.getCaloric()).thenReturn(300d);
        when(patty4.getCaloric()).thenReturn(300d);
        when(patty1.getFlourType()).thenReturn(Patty.FlourType.FIRST_KIND);
        when(patty2.getFlourType()).thenReturn(Patty.FlourType.SECOND_KIND);
        when(patty3.getFlourType()).thenReturn(Patty.FlourType.FIRST_KIND);
        when(patty4.getFlourType()).thenReturn(Patty.FlourType.SECOND_KIND);
        Patty[] actual = new Patty[]{patty3, patty4, patty2, patty1};

        Arrays.sort(actual, new PattyComparatorByCaloric().thenComparing(new PattyComparatorByFlourType()));

        assertArrayEquals("The array must be sorted in ascending order by caloric and flour type.", expected, actual);
    }
}
